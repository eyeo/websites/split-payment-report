const fs = require('fs');
const parse = require('csv-parse/lib/sync');
const test = require('chi-square-a-b-testing');

const start = new Date(process.argv[2]);

if (isNaN(start))
{
  console.error('Invalid start date');
  process.exit(1);
}

console.info(`From ${start}`);

const end = new Date(process.argv[3] || Date.now());

if (isNaN(end))
{
  console.error('Invalid end date');
  process.exit(1);
}

console.info(`To ${end}`);

// creating a table of variant [[unique pageviews, conversions]]
const results = fs.readFileSync('./uniques.txt', 'utf8')
  .split("\n")
  .map(uniques => [parseInt(uniques.replace(',',''), 10), 0])

if (
  // this script only supports a/b tests
  results.length != 2
  || Number.isNaN(results[0])
  || results[0] < 0
  || Number.isNaN(results[1])
  || results[1] < 0
) {
  console.error('Invalid uniques.txt');
  process.exit(1);
}

const stripePayments = parse(fs.readFileSync('./stripe.csv', 'utf8'));

const STRIPE_DATE_INDEX = stripePayments[0].indexOf('Created (UTC)');
const STRIPE_CUSTOM_INDEX = stripePayments[0].indexOf('custom (metadata)');

if (
  STRIPE_DATE_INDEX == -1
  || STRIPE_CUSTOM_INDEX == -1
) {
  console.error('Invalid stripe.csv');
  process.exit(1);
}

for (let i = 1, STRIPE_LENGTH = stripePayments.length; i < STRIPE_LENGTH; i++)
{
  let payment = stripePayments[i];

  let session = payment[STRIPE_CUSTOM_INDEX];

  if (
    // subscription payment sessions will be empty
    session.length < 2
    // valid payment sessions start with ${variant-char}-
    || session[1] !== '-'
    // ignore non-experiment sessions
    // experiment sessions start with 0-N, non-experiment sessions start with x
    || isNaN(session[0])
  ) continue;

  // Stripe export dates are UTC/GMT if README instructions are followed
  let date = new Date(stripePayments[i][STRIPE_DATE_INDEX] + ' GMT');
  if (date < start) continue;
  if (date > end) continue;

  let variant = parseInt(session[0], 10);

  results[variant][1] = results[variant][1] + 1;
}

const paypalPayments = parse(
  // PayPal report CSVs begin with a BOM character 🤦
  fs.readFileSync('./paypal.csv', 'utf8').replace(/^\ufeff/, ''),
);

const PAYPAL_DATE_INDEX = paypalPayments[0].indexOf('Date');
const PAYPAL_TIME_INDEX = paypalPayments[0].indexOf('Time');
const PAYPAL_TYPE_INDEX = paypalPayments[0].indexOf('Type');
const PAYPAL_CUSTOM_INDEX = paypalPayments[0].indexOf('Custom Number');

if (
  PAYPAL_DATE_INDEX == -1
  || PAYPAL_TIME_INDEX == -1
  || PAYPAL_TYPE_INDEX == -1
  || PAYPAL_CUSTOM_INDEX == -1
) {
  console.error('Invalid paypal.csv');
  process.exit(1);
}

for (let i = 1, paypalLength = paypalPayments.length; i < paypalLength; i++)
{
  let payment = paypalPayments[i];

  let session = payment[PAYPAL_CUSTOM_INDEX];

  if (
    payment[PAYPAL_TYPE_INDEX] != 'Website Payment'
    || session.length < 2
    || session[1] !== '-'
    || isNaN(session[0])
  ) continue;

  // PayPal payment dates are DD/MM/YYYY and times are GMT+2
  // PayPal payment date and time zone and format may be changed in PayPal admin
  let date = paypalPayments[i][PAYPAL_DATE_INDEX].split('/');
  date = new Date(
    `${date[2]}/${date[1]}/${date[0]} ${paypalPayments[i][PAYPAL_TIME_INDEX]} GMT+2`
  );
  if (date < start) continue;
  if (date > end) continue;

  let variant = parseInt(session[0], 10);

  results[variant][1] = results[variant][1] + 1;
}

for (let i = 0; i < results.length; i++)
{
  if (results[i][0] < 5)
  {
    console.error(`Variant ${i} didn't get enough uniques to calculate results`);
    process.exit(1);
  }

  if (results[i][1] < 5)
  {
    console.error(`Variant ${i} didn't get enough conversions to calculate results`);
    process.exit(1);
  }
  
  results[i].push(Math.round((results[i][1]/results[i][0]) * 100000) / 1000);

  console.info(`Variant ${i} / Uniques ${results[i][0]} / Conversions ${results[i][1]} / Rate ${results[i][2]}%`)
}

const pValue = Math.round(test(results) * 1000) / 10;

console.info(`We're ${pValue}% certain that there's a difference between the two.`);