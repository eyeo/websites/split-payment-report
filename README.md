# Payment split test report

Report on the results of a split test of a payment page.

## Usage

1. Clone repo
1. npm install
1. Download PayPal activity
    1. Activity > All Reports > Activity download 
    1. Completed payments in experiment date range in CSV format
    1. Move csv to `./paypal.csv`
1. Download stripe activity
    1. Payments > Export
    1. UTC, Custom range, All columns
    1. Move csv to `./stripe.csv`
1. Copy analytics activity
    1. Goto Experiment > Reporting > View in Analytics
    1. Change Conversions / Page Metrics to Page Metrics
    1. Copy Original and Variant 1 Unique Pageviews to `./uniques.txt`
        1. Original on line 0 and Variant on line 1
1. Run `node report.js "$START" "$END"`
    1. Where `$START` is the start date and time of the experiment
        1. You can copy/paste this from the experiment reporting in optimize
    1. Where `$END` is the end date and time of the experiment
        1. You can copy/paste this from the experiment reporting in optimize
        1. `$END` is optional. It defaults to the current date and time.
1. See results in console

**Caution**: All of these sources may be 2-12+ hours behind.

## Limitations

- It doesn't count subscriptions
- It doesn't account for disputes or refunds
- It doesn't calculate which variant will perform better
    - It calculates the probability that Variant 0 will produce the results of Variant 1
- It's results are not foolproof e.g.
    - Server and page performance variation, cracking attempts, and irregular traffic patterns may invalidate them. 

## Acknowledgements

Relies on:

- `chi-square-a-b-testing`
    - The JavaScript implementation of chi-square test was done by http://stats.theinfo.org/ ([Aaron Swartz](http://www.aaronsw.com/) and [Ben Wikler](https://twitter.com/benwikler))
- `csv-parse`